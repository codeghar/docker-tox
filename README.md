# tox in a container

Use ``tox`` with multiple Python versions in the same container.

## Why?

The upstream [Python Docker images](https://hub.docker.com/_/python/) come with one version of Python installed per
image.  This repo can create a single image that has multiple Python versions installed for use by ``tox``.

## Use

There are two ways to use this repo: ``make`` or ``docker-compose``.

If you prefer to use ``docker-compose``, you can use its commands instead of ``make``. One caveat, though, is that
``docker-compose`` does not yet support the *--squash* flag supported by ``docker build``. This is why I recommend
that you use ``make`` as below.

        $ make build

Build a Docker image that contains multiple Python versions and has ``tox`` installed. It may take more than 30 minutes
to build the image.

        $ make start

Start a container from the image.

        $ make exec

Start a bash session in the container.

        $ make stop

Stop the container. The container will also be deleted.

## pkg_install.conf

When you build an image, pkgsrc will fetch information about
[vulnerabilities](http://codeghar.com/blog/essential-pkgsrc-the-missing-mini-handbook.html#vulnerabilities).
Since *mk.conf* is set to not allow a package to be built if it has known vulnerabilities, the build will fail if new
(previously unknown) vulnerabilities are found. This file allows us to selectively ignore some known vulnerabilities.

## Why pkgsrc?

pkgsrc is a cross operating system package manager. It supports -- among many others -- NetBSD, Minix, SmartOS, Linux,
and macOS. I like it because of this portability. It provides a large number of different packages. I have never
encountered a package that I needed but was not available. In short pkgsrc is a portable, featureful, and flexible
package manager. What's not to like?

pkgsrc can sometimes be a little behind native package managers, such as MacPorts on macOS, but it catches up quickly
if you follow its *trunk* branch. For this use case it works well enough.

## Why multiple layers?

The Dockerfile is setup to create a lot of intermediate layers. The reason is so we don't need to repeat expensive
operations if a step in the middle fails during a build.

One reason for failure might be new vulnerabilities discovered. These would then need to be evaluated and optionally
ignored in [pkg_install.conf](http://codeghar.com/blog/essential-pkgsrc-the-missing-mini-handbook.html#vulnerabilities).
If that happens, we don't want to repeat the time consuming ``git clone`` and ``./bootstrap`` steps.
