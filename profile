export PATH
PATH="/usr/pkg/sbin:/usr/pkg/bin:${PATH}"

export MANPATH
MANPATH="/usr/pkg/man:${MANPATH}"

export LC_CTYPE
LC_CTYPE="en_US.UTF-8"
export LC_COLLATE
LC_COLLATE="C"
export LC_TIME
LC_TIME="C"
export LC_NUMERIC
LC_NUMERIC="C"
export LC_MONETARY
LC_MONETARY="C"
export LC_MESSAGES
LC_MESSAGES="en_US.UTF-8"
export LC_ALL
LC_ALL=""
