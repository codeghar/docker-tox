IMG = localhost/tox:latest

.PHONY: build
build:
	docker build -t $(IMG) .
	# docker-compose build tox

.PHONY: exec
exec:
	docker exec -it tox bash
	# docker-compose exec tox bash

.PHONY: start
start:
	docker run --detach --rm --tty --name tox $(IMG) bash
	# docker-compose up -d tox

.PHONY: stop
stop:
	docker stop tox
	# docker-compose down

.PHONY: push
push:
	docker tag $(IMG) codeghar/tox:latest
	docker push codeghar/tox:latest
