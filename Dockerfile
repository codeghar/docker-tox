FROM ubuntu:18.04 as builder
RUN    apt update \
    && apt install -y build-essential git locales \
    && apt upgrade -y \
    && locale-gen en_US.UTF-8 \
    && dpkg-reconfigure --frontend noninteractive locales
RUN git clone --branch trunk --single-branch --depth=1 https://github.com/NetBSD/pkgsrc.git /root/pkgsrc
RUN    export SH=/bin/bash \
    && cd ~/pkgsrc/bootstrap \
    && ./bootstrap
RUN    mkdir -p /root/pkg/work \
    && mkdir -p /root/pkg/distfiles
COPY profile /etc/profile.d/pkgsrc
COPY mk.conf /usr/pkg/etc/mk.conf
COPY pkg_install.conf /usr/pkg/etc/pkg_install.conf
RUN    . /etc/profile.d/pkgsrc \
    && pkg_admin -K /usr/pkg/pkgdb fetch-pkg-vulnerabilities
RUN    . /etc/profile.d/pkgsrc \
    && cd /root/pkgsrc/pkgtools/pkg_alternatives \
    && bmake install clean clean-depends
RUN    . /etc/profile.d/pkgsrc \
    && cd /root/pkgsrc/lang/python37 \
    && bmake install clean clean-depends \
    && cd /root/pkgsrc/devel/py-pip \
    && bmake install clean clean-depends \
    && cd /root/pkgsrc/devel/py-virtualenv \
    && bmake install clean clean-depends
RUN    . /etc/profile.d/pkgsrc \
    && cd /root/pkgsrc/lang/python27 \
    && bmake install clean clean-depends \
    && cd /root/pkgsrc/devel/py-pip \
    && bmake install clean clean-depends PYTHON_VERSION_DEFAULT=27 \
    && cd /root/pkgsrc/devel/py-virtualenv \
    && bmake install clean clean-depends PYTHON_VERSION_DEFAULT=27
RUN    . /etc/profile.d/pkgsrc \
    && cd /root/pkgsrc/lang/python35 \
    && bmake install clean clean-depends \
    && cd /root/pkgsrc/devel/py-pip \
    && bmake install clean clean-depends PYTHON_VERSION_DEFAULT=35 \
    && cd /root/pkgsrc/devel/py-virtualenv \
    && bmake install clean clean-depends PYTHON_VERSION_DEFAULT=35
RUN    . /etc/profile.d/pkgsrc \
    && cd /root/pkgsrc/lang/python36 \
    && bmake install clean clean-depends \
    && cd /root/pkgsrc/devel/py-pip \
    && bmake install clean clean-depends PYTHON_VERSION_DEFAULT=36 \
    && cd /root/pkgsrc/devel/py-virtualenv \
    && bmake install clean clean-depends PYTHON_VERSION_DEFAULT=36
RUN tar czf /root/pkg.tgz /usr/pkg

FROM ubuntu:18.04 as distro
RUN    apt update \
    && apt install -y locales \
    && apt upgrade -y \
    && locale-gen en_US.UTF-8 \
    && dpkg-reconfigure --frontend noninteractive locales \
    && rm -rf /var/lib/apt/lists/*
COPY --from=builder /root/pkg.tgz /root/pkg.tgz
RUN tar xzf /root/pkg.tgz -C /usr --strip=1
COPY profile /etc/profile.d/pkgsrc
COPY bashrc_pkgsrc_edits.sh /root
RUN    chmod +x /root/bashrc_pkgsrc_edits.sh \
    && /root/bashrc_pkgsrc_edits.sh
RUN rm -rf /root/bashrc_pkgsrc_edits.sh /root/pkg.tgz /etc/profile.d/pkgsrc
RUN /usr/pkg/bin/python3.7 -m pip install tox
